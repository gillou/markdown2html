# encoding: utf-8

from os import path
import re, sys
from shutil import copyfile
 
def splitfn(fn):
    """Split file full path in path, name, ext"""
    fpath, fn = path.split(fn)
    name, ext = path.splitext(fn)

    return fpath, name, ext

def remove_accent(texte):
    # Supprime les accents → Minuscule
    accents = "ŠŽšžŸÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðñòóôõöùúûüýÿ"
    no_accents = "SZszYAAAAAACEEEEIIIIDNOOOOOUUUUYaaaaaaceeeeiiiidnooooouuuuyy"
    lettres = list(texte)
    for i,lettre in enumerate(lettres):
        idx = accents.find(lettre)
        if idx>-1:
            lettres[i] = no_accents[idx]
    
    return "".join(lettres)


def slugify(texte, split_camel=True):
    """Transforme une chaine en slug pour être URL friendly
       Les cas minimaux sont gérés :
       - Suppression des accents (appel de remove_accent)
       - Ajout d'espace dans les mots en camel case (option True par défaut)
       - Ajout de tiret dans les espaces
       - Passage en minuscule
    """
    str_prepare = texte

    if split_camel:
        # Ajoute un espace avant les majuscules pour séparer les camelcase
        str_prepare = ''.join([' {}'.format(i) if i.isupper() else i for i in texte])

        # Remplace des 2 espaces par un simple (cas où un mot est espacé avec majuscule)
        str_prepare = str_prepare.replace('  ', ' ')
    
    # Supprime les accents → Minuscule
    str_prepare = remove_accent(" ".join(str_prepare.split("  ")).strip().lower())
    
    # Remplace les espaces par des tirets
    str_out = "-".join(str_prepare.split(" "))
    str_out = re.sub('-+', '-', str_out)

    return str_out 

