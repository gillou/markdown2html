#!/usr/bin/env python
# encoding: utf-8

import sys
import re
from os import path, mkdir, getcwd
from shutil import copyfile, rmtree
# from glob import glob
import tkinter as tk  # Python 3
from tkinter import ttk, StringVar
import subprocess
import tempfile
from pathlib import Path

from utils import splitfn, remove_accent, slugify
from convert_md_to_html import convert_md_to_html

def write_index(l_files):
    """Write index.html for navigation amng generated files
    """



class App:
    '''List and Choose Markdown files to generate html files using pandoc
    A directory for each selected filename converted as slug is created containing the converted page (index.html+images)
    '''
    def __init__(self, 
                base_dir=None,
                out_dir=None):
        """Init generation application
            Inputs : 
            base_dir -- base directory to look for md files
            out_dir -- output directory to save html created pages
        """

        # Input analysis
        if base_dir is None:
            self.base_dir = out_dir = getcwd() + path.sep
        else:
            self.base_dir = base_dir
        
        self.out_dir = out_dir
        self.l_md_files = [str(item) for item in Path(self.base_dir).rglob('*.md')]

        # Create main interface
        root = tk.Tk()
        root.title('MD Site Creator')
        
        # Create Window with one row
        self.win = win = ttk.PanedWindow(root, orient=tk.HORIZONTAL, heigh=250, width=100)
        self.win.pack(fill=tk.BOTH, expand=1)
        left = ttk.Frame(win)
        win.add(left)

        # Select dir button + label
        self.txt_base_dir = StringVar()
        self.label_dir = ttk.Label(root, textvariable=self.txt_base_dir)
        self.txt_base_dir.set(self.base_dir)
        self.label_dir.pack()
        btn_selectdir = ttk.Button(root, text='Dossier des md ...', command=self.select_dir) 
        btn_selectdir.pack()

        # Select output dir button + label
        self.txt_out_dir = StringVar()
        self.label_outdir = ttk.Label(root, textvariable=self.txt_out_dir)
        self.txt_out_dir.set(self.out_dir)
        self.label_outdir.pack()
        btn_selectoutdir = ttk.Button(root, text='Dossier de publication ...', command=self.select_outdir) 
        btn_selectoutdir.pack()

        # Creating a List (with vertical scrollbar)
        scrollbar = ttk.Scrollbar(left, orient=tk.VERTICAL)
        self.lb_mdfiles = lb_mdfiles = tk.Listbox(left, yscrollcommand=scrollbar.set, width = 50, height=25, selectmode=tk.MULTIPLE)
        scrollbar.config(command=lb_mdfiles.yview)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        lb_mdfiles.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)
        
        # Populate list with 
        for fn in self.l_md_files:
            name = splitfn(fn)[1]
            lb_mdfiles.insert(tk.END, name)

        # Generate button
        btn_generate = ttk.Button(root, text='Générer', command=self.generate) 
        btn_generate.pack()

    def select_dir(self):
        """Open GUI to select dir for listing markdown files"""

        # Select directory
        root = tk.Tk()
        root.directory = tk.filedialog.askdirectory(initialdir=self.base_dir)
        
        if root.directory == '':
            print('Abandon')
            return

        # TODO: Update interface 
        self.update_dir(root.directory)

    def select_outdir(self):
        """Open GUI to select outdir for listing markdown files"""
        
        # Select output directory
        root = tk.Tk()
        root.directory = tk.filedialog.askdirectory(initialdir=self.out_dir)
        
        if root.directory == '':
            print('Abandon')
            root.destroy
            return

        # Update interface 
        self.out_dir = root.directory + path.sep
        self.txt_out_dir.set(self.out_dir)

        root.destroy
        return

    def update_dir(self, new_dir):
        """Update md file list when a new directory is selected"""
        
        # Stocke de répertoire saisie dans l'objet
        self.base_dir = new_dir
        
        # liste les fichiers md dans l'arborescence
        self.l_md_files = [str(item) for item in Path(self.base_dir).rglob('*.md')]
        
        # Efface la liste précédente
        self.lb_mdfiles.delete(0, self.lb_mdfiles.size()-1)
        self.txt_base_dir.set(self.base_dir)

        # Remplit la liste avec les fichiers de l'arborescence 
        for fn in self.l_md_files:
            name = splitfn(fn)[1]
            self.lb_mdfiles.insert(tk.END, name)


    def generate(self):
        """Main generation function
        Runs through the list of selected md files and convert them
        using convert_md_to_html
        """

        # Check if any file selected
        if len(self.lb_mdfiles.curselection())==0:
            return

        # Get selected file and convert them to html in subdirectory
        lselect = self.lb_mdfiles.curselection()
        lgenerated = []
        for idx in lselect:
            print("\n---\nSelection : {} : {}".format(idx, self.l_md_files[idx]))
            tmp, nom_page, rel_link = convert_md_to_html(in_file=self.l_md_files[idx], 
                    out_dir= self.out_dir, 
                    template_css="../css/template.css",
                    create_subdir=True,
                    nav_back=True)
            
            if tmp != "":
                lgenerated.append([nom_page, rel_link])

        if len(lgenerated)>0:
            # Create index.html with list of files in root dir
            temp_dir = tempfile.mkdtemp()
            index_md = path.join(temp_dir, 'index.md')

            print('\nCreating index.md for navigation in temp dir:', index_md)

            # Ecriture du fichier md 
            with open(index_md, mode='wt', encoding='utf-8') as fic:
                str_head = '''---
title: "Liste des fichiers convertis"
author: [Gilles Gonon]
---

# Liste des fichiers disponibles

'''
                fic.write(str_head)
                
                for pages in lgenerated:
                    str_line = "- [{}]({})\n".format(pages[0], pages[1])
                    fic.write(str_line)
            
            print('Generating index.html for navigation:')
            convert_md_to_html(in_file=index_md, 
                    out_dir=self.out_dir, 
                    template_css="css/template.css",
                    create_subdir=False, 
                    nav_back=False)
            
            # Supprime les fichiers temp créés
            print('Deleting temp files: ', temp_dir) 
            rmtree(temp_dir)

        print('\n---\n Site created\n')
        

    def run(self):
        tk.mainloop()

if __name__ == '__main__':
    print(sys.argv)
    if len(sys.argv)==1:
        App(base_dir="C:/Users/gille/OneDrive/Documents/IMTS/Notes/", 
            out_dir="C:/Users/gille/OneDrive/Documents/IMTS/Notes/Publish/site-formation/").run()
    elif len(sys.argv)==3:
        App(base_dir=sys.argv[1], 
            out_dir=sys.argv[2]).run()
    else:
        print(App.__doc__, App.__init__.__doc__, sep='\n')

