# Génération de site à partir de fichier Markdown

## Introduction

J'utilise depuis 1 an l'outil [Zettlr](https://www.zettlr.com/) pour la prise de notes au format Markdown. Je le trouve très pratique et efficace et ce qui m'a attiré en première instance est la possibilité d'exporter les fichiers Markdown vers de nombreux formats en 1 clics. Idéal donc pour la prise de note rapide et pour un export de qualité dans différents formats (html, doc, odt, pdf, latex, revealjs).

Voici deux utilitaires pour personnaliser la génération de site : 

- `convert_md_to_html.py` : conversion unitaire d'un fichier md → HTML. 
- `create_site.py` : conversion unitaire d'un fichier md → HTML. 
Cet utilitaire Python permet de choisir un dossier pour lister les fichiers markdown qu'il contient
et générer un site à partir des pages sélectionnées.

Le lanceur est `create_site.py` a été ajouté (selon le modèle des exemples opencv).

```bash
python convert_md_to_html.py
```
