# encoding: utf-8
import tkinter as tk  # Python 3
from tkinter import filedialog
import subprocess
from os import path, mkdir, getcwd, makedirs
import re, sys
from shutil import copyfile, copytree
from utils import remove_accent, slugify
from urllib.parse import unquote 
from distutils.dir_util import copy_tree

# Some files are relatives to this script dir
script_dir = path.dirname(path.realpath(__file__))

def convert_md_to_html(in_file=None, 
                    out_dir=None, 
                    template_html=path.join(script_dir, "template.html"), 
                    template_css="css/template.css",
                    create_subdir=True,
                    nav_back=False):
    """Call pandoc bin to generate a html file from Markdown file using given template.
    
    in_file -- input md file to convert, if None given, ask the user to select one
    out_dir -- output_dir to save the generated page 
    template_html -- base name for html template. It uses pandoc syntax, 
                     see: https://pandoc.org/MANUAL.html#templates
                     By default use template.html in python source folder
    template_css  -- relative name for css template,
    create_subdir -- flag to generate files in subdir, default True
    nav_back -- flag to add a back link in navbar, default False
    """
    
    # Check inputs
    if out_dir is None:
        out_dir = getcwd() + path.sep
    
    start_dir = path.dirname(out_dir[:-1])
    
    # Choix du fichier
    if in_file is None:
        root = tk.Tk()
        root.filename = filedialog.askopenfilename(initialdir = start_dir, 
                            title = u"choisir le markdown à convertir",
                            filetypes = (("markdown files","*.md"),("all files","*.*")))
        
        if root.filename == '':
            print('Abandon')
            return '', '', ''

        print (root.filename)
        in_file = root.filename

    # Formattage des noms en sortie ()
    file_dir = path.dirname(in_file)
    nom_page = path.splitext(path.basename(in_file))[0]

    nom_slug = slugify(nom_page)
    if create_subdir:
        # on crée un sous-dossier pour sauver la page
        save_dir = out_dir + nom_slug
        if not path.exists(save_dir):
            makedirs(save_dir)

        # Nom + chemin complet de la page de sortie
        out_file = '{0}/index.html'.format(save_dir)
        relative_link = '{}/index.html'.format(nom_slug)
    else:
        save_dir = out_dir
        out_file = '{0}/{1}.html'.format(save_dir, nom_slug)
        relative_link = '{}.html'.format(nom_slug)


    # Appel de la commande pandoc
    cmdPandoc = 'pandoc --verbose "{0}" -o "{1}" --template "{2}" --css "{3}" --toc --toc-depth 2'.format(in_file, out_file, template_html, template_css)
    if nav_back:
        cmdPandoc += ' -V back'

    print(u'Lancement de la commande \n' + cmdPandoc)

    obj_run = subprocess.run(cmdPandoc, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if obj_run.returncode != 0:
        print("Erreur:", obj_run.returncode, file=obj_run.stdout)
        print("Abandon de la génération")
        return '', '', ''

    # Lecture du fichier HTML généré mour récupérer les images liées 
    # dans la page et les recopier vers le dossier
    with open(out_file, mode='r', encoding='utf-8') as html:
        content = html.read()
        # Matches contient une liste des images à recopier (recherche de 'img src=')
        matches = re.findall(r'img\ssrc="([^"]+)"', content)

        print('Copie des {} images dans le dossier {}'.format(len(matches), save_dir) )
        for img in matches:

            img = unquote(img)
            # Test si l'image est dans un sous-dossier
            if path.dirname(img) != '' and not path.exists(path.join(save_dir, path.dirname(img))):
                print(' - create dir ', path.join(save_dir, path.dirname(img)))
                makedirs(path.join(save_dir, path.dirname(img)))
            
            print(' - {} → {}'.format(file_dir+'/'+img, save_dir+'/'+img) )        
            try:
                copyfile(file_dir+'/'+img, save_dir+'/'+img, follow_symlinks=False)
            except FileNotFoundError:
                 print(u"ERREUR : Fichier non trouvé (à copier manuellement)")
    
    # Copy css and img ressources
    copy_tree(path.join(script_dir, 'css'), path.join(out_dir, 'css'))
    copy_tree(path.join(script_dir, 'img'), path.join(out_dir, 'img'))

    # Return generated filename (for navigation)
    # print(' → ', out_file)
    return out_file, nom_page, relative_link

if __name__ == '__main__':
    # Affichage de la doc
    print(convert_md_to_html.__doc__)
    # Lancement de la fonction de conversion
    convert_md_to_html(in_file=None, 
                    out_dir="C:/Users/gille/OneDrive/Documents/IMTS/Notes/Publish/", 
                    template_css="../css/template.css")